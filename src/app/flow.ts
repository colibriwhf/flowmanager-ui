export class Flow {

    constructor(
        public id: number,
        public flowName: string,
        public flowOwner: string,
        public active: boolean,
        public isOldVersion: boolean,
        public creationDate: string,
        public fileFormat: string,
        public lastUpdateDate: string,
        public retention: number,
        public sensorId: number,
        public visibility: string,
        public schemaId: string
    ){}
}


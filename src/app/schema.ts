export class Schemas {

    constructor(
        public entities: Array<Schema>
    ){}
}

export class Schema {

    constructor(
        public schemaMetadata: SchemaMetadata,
        public id: number,
        public timestamp: number
    ){}
}

export class SchemaMetadata {
    constructor(
        public type: string,
        public schemaGroup: string,
        public name: string,
        public description: string,
        public compatibility: string,
        public validationLevel: string,
        public evolve: boolean
    ){}
}

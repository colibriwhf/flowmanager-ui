import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';


const headers = new HttpHeaders()
    .set('content-type', 'application/json')
    .set('Access-Control-Allow-Origin', '*');


@Injectable({
    providedIn: 'root'
})
export class SchemaDataService {

    // This url is for schema registry
    baseUrl = 'http://localhost:8080/schema';

    constructor(private httpClient: HttpClient) {
    }

    getSchemas() {
        return this.httpClient.get(this.baseUrl);
    }
}

import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {Flow} from '../flow';
import {Observable, throwError} from 'rxjs';
import {catchError, retry} from 'rxjs/operators';


const headers = new HttpHeaders()
    .set('content-type', 'application/json')
    .set('Access-Control-Allow-Origin', '*');


@Injectable({
    providedIn: 'root'
})
export class FlowDataService {


    // baseUrl: string = "http://147.135.253.223:10002";
    baseUrl = 'http://localhost:8080/flow';

    constructor(private httpClient: HttpClient) {
    }

    getFlowList() {
        return this.httpClient.get<Flow[]>(this.baseUrl);
        // .pipe(
        //   catchError(this.handleError)
        // );
    }

    saveFlow(flow: Flow): Observable<Flow> {
        return this.httpClient.post<Flow>(this.baseUrl, flow, {headers});
        // .pipe(
        //   catchError(this.handleError)
        // );
    }

    updateStatusFlow(flow: Flow, status: boolean): Observable<Flow> {
        return this.httpClient.put<Flow>(this.baseUrl + '/' + flow.id + '/' + status, flow, {headers});
    }

    updateFlow(flow: Flow): Observable<Flow> {
        return this.httpClient.put<Flow>(this.baseUrl + '/' + flow.id, flow, {headers});
        // .pipe(
        //   catchError(this.handleError)
        // );
    }

    deleteFlow(flowId: number): Observable<{}> {
        return this.httpClient.delete(this.baseUrl + '/' + flowId, {headers});
        // .pipe(
        //   catchError(this.handleError)
        // );
    }

    private handleError(error: HttpErrorResponse) {
        if (error.error instanceof ErrorEvent) {
            // A client-side or network error occurred. Handle it accordingly.
            console.error('An error occurred:', error.error.message);
        } else {
            // The backend returned an unsuccessful response code.
            // The response body may contain clues as to what went wrong,
            console.error(
                `Backend returned code ${error.status}, ` +
                `body was: ${error.error}`);
        }
        // return an observable with a user-facing error message
        return throwError(
            'Something bad happened; please try again later.');
    };

}

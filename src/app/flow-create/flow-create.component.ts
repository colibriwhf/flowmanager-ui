import { Component, OnInit } from '@angular/core';
import { FlowDataService } from '../service/flow-data.service';
import { Flow } from '../flow';
import { FlowFileFormat } from '../flow-file-format.enum';
import { Router } from '@angular/router';
import {SchemaDataService} from '../service/schema-data.service';

@Component({
  selector: 'app-flow-create',
  templateUrl: './flow-create.component.html',
  styleUrls: ['./flow-create.component.css']
})
export class FlowCreateComponent implements OnInit {

  updateFlag = false;
  flow = new Flow(null, '', '', true, false, '', FlowFileFormat.JSON, '', 2, 0, '', '');
  keys = Object.keys;
  fileFormats = FlowFileFormat;
  schemas;

  constructor(private flowDataService: FlowDataService,
              private schemaDataService: SchemaDataService,
              private router: Router) {
  }

  ngOnInit(): void {
    if (history.state.flowToEdit !== undefined){
      this.flow = history.state.flowToEdit;
      this.updateFlag = true;
    }
    this.schemaDataService.getSchemas().subscribe(
        (response: Array<string>) => {
          this.schemas = response;
        }
    );
  }

  saveFlow(){
    if (this.updateFlag === false){
      this.flowDataService.saveFlow(this.flow).subscribe(data => {
        this.router.navigate(['flow-list']);
      });
    } else{
      this.flowDataService.updateFlow(this.flow).subscribe(data => {
        this.router.navigate(['flow-list']);
      });
    }

  }

}

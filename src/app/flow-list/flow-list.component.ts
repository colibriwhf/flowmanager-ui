import { Component, OnInit } from '@angular/core';
import { Flow } from '../flow';
import { FlowDataService } from '../service/flow-data.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-flow-list',
  templateUrl: './flow-list.component.html',
  styleUrls: ['./flow-list.component.css']
})
export class FlowListComponent implements OnInit {

  flowList: Flow[];

  constructor(private flowDataService: FlowDataService, private router: Router) { }

  ngOnInit(): void {
    this.flowDataService.getFlowList().subscribe(
      (response) => {
        this.flowList = response;
      }
    );
  }

  editFlow(flow: Flow){
    this.router.navigate(['flow-create'],{
      state: { flowToEdit: flow }
    });
  }


  changeFlowStatus(flow: Flow){
    flow.active = !flow.active;
    this.flowDataService.updateStatusFlow(flow, flow.active).subscribe(data => {
      this.ngOnInit();
    });
  }

  deleteFlow(flowId: number){
    this.flowDataService.deleteFlow(flowId).subscribe(data => {
      this.ngOnInit();
    });
  }

}

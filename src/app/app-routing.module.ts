import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { FlowCreateComponent } from './flow-create/flow-create.component';
import { FlowListComponent } from './flow-list/flow-list.component';


//const routes: Routes = [];
const routes: Routes = [
  {path:  "", pathMatch:  "full", redirectTo: "flow-list"},
  {path: "home", component: HomeComponent},
  {path: "flow-create", component: FlowCreateComponent},
  {path: "flow-list", component: FlowListComponent}  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

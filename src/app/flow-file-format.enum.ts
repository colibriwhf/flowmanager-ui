export enum FlowFileFormat {
    JSON = 'JSON',
    XML = 'XML',
    CSV = 'CSV'
}

export class UserFile {
    constructor(fileHash, fileName, hdfsURI, schemaApplied, user) {
        this.fileHash = fileHash;
        this.fileName = fileName;
        this.hdfsURI = hdfsURI;
        this.schemaApplied = schemaApplied;
        this.user = user;
    }
}
//# sourceMappingURL=user-file.js.map
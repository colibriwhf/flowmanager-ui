import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
let UserFileDataService = class UserFileDataService {
    constructor(httpClient) {
        this.httpClient = httpClient;
        // baseUrl = 'http://193.70.12.230:8080';
        this.baseUrl = 'http://localhost:8080';
    }
    getUserFile(user, hash) {
        return this.httpClient.get(this.baseUrl +
            '/user/file/getByUserAndHash/' + user + '&' + hash);
    }
    retrieveFile(userFile) {
        return this.httpClient.post(this.baseUrl + '/user/file/retrieve', userFile)
            .subscribe();
    }
    removeFile(userFile) {
        return this.httpClient.post(this.baseUrl + '/user/file/remove', userFile)
            .subscribe();
    }
};
UserFileDataService = __decorate([
    Injectable({
        providedIn: 'root'
    })
], UserFileDataService);
export { UserFileDataService };
//# sourceMappingURL=user-file-data.service.js.map
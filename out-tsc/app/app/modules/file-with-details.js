export class FileWithDetails {
    constructor(file, 
    // tslint:disable-next-line:variable-name
    _fileDetails) {
        this.file = file;
        this._fileDetails = _fileDetails;
    }
    set fileDetails(value) {
        this._fileDetails = value;
    }
}
//# sourceMappingURL=file-with-details.js.map